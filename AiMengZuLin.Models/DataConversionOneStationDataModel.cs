﻿using AiMengZuLin.Enums;
using Microsoft.Toolkit.Mvvm.ComponentModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Media.Imaging;

namespace AiMengZuLin.Models
{
    public class DataConversionOneStationDataModel : ObservableObject
    {
        private DataConversionOneDataTypeEnum _dataType;
        public DataConversionOneDataTypeEnum DataType
        {
            get
            {
                return _dataType;
            }
            set
            {
                SetProperty(ref _dataType, value);
            }
        }

        private int _year;
        public int Year
        {
            get
            {
                return _year;
            }
            set
            {
                SetProperty(ref _year, value);
            }
        }

        private int _month;
        public int Month
        {
            get
            {
                return _month;
            }
            set
            {
                SetProperty(ref _month, value);
            }
        }

        private int _day;
        public int Day
        {
            get
            {
                return _day;
            }
            set
            {
                SetProperty(ref _day, value);
            }
        }

        private decimal _preData20_20;
        public decimal PreData20_20
        {
            get
            {
                return _preData20_20;
            }
            set
            {
                SetProperty(ref _preData20_20, value);
            }
        }

        private decimal _temMaxData;
        public decimal TemMaxData
        {
            get
            {
                return _temMaxData;
            }
            set
            {
                SetProperty(ref _temMaxData, value);
            }
        }

        private decimal _temMinData;
        public decimal TemMinData
        {
            get
            {
                return _temMinData;
            }
            set
            {
                SetProperty(ref _temMinData, value);
            }
        }
    }
}
