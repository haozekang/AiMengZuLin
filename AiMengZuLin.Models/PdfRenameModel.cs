﻿using Microsoft.Toolkit.Mvvm.ComponentModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Media.Imaging;

namespace AiMengZuLin.Models
{
    public class PdfRenameModel : ObservableObject
    {
        private string _name;
        public string Name 
        {
            get
            {
                return _name;
            }
            set
            {
                SetProperty(ref _name, value);
            }
        }

        private string _fullName;
        public string FullName
        {
            get
            {
                return _fullName;
            }
            set
            {
                SetProperty(ref _fullName, value);
            }
        }

        private string _newName;
        public string NewName
        {
            get
            {
                return _newName;
            }
            set
            {
                SetProperty(ref _newName, value);
            }
        }

        private BitmapImage _ocrImage;
        public BitmapImage OcrImage
        {
            get
            {
                return _ocrImage;
            }
            set
            {
                SetProperty(ref _ocrImage, value);
            }
        }
    }
}
