﻿using SharpYaml.Serialization;

namespace AiMengZuLin.ConfigModels
{
    public class ConfigInfo
    {
        [YamlMember("Databases")]
        public DatabasesInfo Databases { get; set; }

        [YamlMember("System")]
        public SystemInfo System { get; set; }
    }
}
