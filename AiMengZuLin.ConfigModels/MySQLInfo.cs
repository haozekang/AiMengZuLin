﻿using SharpYaml.Serialization;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AiMengZuLin.ConfigModels
{
    public class MySQLInfo
    {
        [YamlMember("Name")]
        public string Name { get; set; }

        [YamlMember("Host")]
        public string Host { get; set; }

        [YamlMember("Port")]
        public int? Port { get; set; }

        [YamlMember("Username")]
        public string Username { get; set; }

        [YamlMember("Password")]
        public string Password { get; set; }

        [YamlMember("Charset")]
        public string Charset { get; set; }

        [YamlMember("UseSsl")]
        public bool? UseSsl { get; set; }

        public string GetConnectionString()
        {
            string s = $"Data Source={Host};";
            s = s + $"Port={Port};";
            s = s + $"Initial Catalog={Name};";
            s = s + $"User Id={Username};";
            s = s + $"Password={Password};";
            s = s + $"Charset={Charset};";
            if (UseSsl == null || UseSsl == false)
            {
                s = s + $"SslMode=None;";
            }
            return s;
        }
    }
}
