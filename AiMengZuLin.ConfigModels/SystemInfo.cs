﻿using SharpYaml.Serialization;

namespace AiMengZuLin.ConfigModels
{
    public class SystemInfo
    {
        [YamlMember("Title")]
        public string Title { get; set; }
    }
}