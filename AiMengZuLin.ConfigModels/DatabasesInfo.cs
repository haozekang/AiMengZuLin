﻿using SharpYaml.Serialization;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AiMengZuLin.ConfigModels
{
    public class DatabasesInfo
    {
        [YamlMember("MySQL")]
        public MySQLInfo? MySQL { get; set; }
    }
}
