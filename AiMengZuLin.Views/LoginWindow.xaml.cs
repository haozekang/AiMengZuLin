﻿using AiMengZuLin.ExtendMethod;
using MahApps.Metro.Controls;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;

namespace AiMengZuLin.Views
{
    /// <summary>
    /// LoginWindow.xaml 的交互逻辑
    /// </summary>
    public partial class LoginWindow : MetroWindow
    {
        public bool? IsLoginSuccessed 
        {
            get
            {
                return this.Tag as bool?;
            }
        }

        public LoginWindow()
        {
            InitializeComponent();

            string icoPath = Path.Combine(Environment.CurrentDirectory, "favicon.ico");
            if (File.Exists(icoPath))
            {
                this.Icon = new BitmapImage(new Uri(icoPath, UriKind.RelativeOrAbsolute));
            }
        }

        private void page_Closing(object sender, System.ComponentModel.CancelEventArgs e)
        {
            if (IsLoginSuccessed != true)
            {
                return;
            }
            MainWindow mw = new MainWindow();
            Application.Current.MainWindow = mw;
            mw.Show();
        }
    }
}
