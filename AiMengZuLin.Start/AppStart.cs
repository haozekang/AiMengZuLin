﻿using AiMengZuLin.Basic;
using AiMengZuLin.ConfigModels;
using AiMengZuLin.Database;
using SharpYaml.Serialization;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace AiMengZuLin.Start
{
    public static class AppStart
    {
        public static void LoadConfig() 
        {
            string configPath = Path.Combine(Environment.CurrentDirectory, "config.yml");
            if (!File.Exists(configPath))
            {
                throw new FileNotFoundException($"未发现config.yml文件！");
            }
            var serializer = new Serializer();
            ConfigInfo Config = null;
            using (Stream f = new FileStream(configPath, FileMode.Open, FileAccess.Read, FileShare.ReadWrite))
            {
                Config = serializer.Deserialize<ConfigInfo>(f);
                AppConfig.SystemTitle = Config?.System?.Title;
                DbHelper.DatabaseConnectionString = Config?.Databases?.MySQL?.GetConnectionString();
            }
        }
    }
}
