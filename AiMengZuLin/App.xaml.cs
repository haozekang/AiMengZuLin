﻿using AiMengZuLin.ConfigModels;
using AiMengZuLin.Start;
using AiMengZuLin.Views;
using AiMengZuLin.ExtendMethod;
using Serilog;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace AiMengZuLin
{
    /// <summary>
    /// Interaction logic for App.xaml
    /// </summary>
    public partial class App : Application
    {
        protected override void OnStartup(StartupEventArgs e)
        {
            Log.Logger = new LoggerConfiguration()
                .MinimumLevel.Verbose()
                .WriteTo.Debug(restrictedToMinimumLevel: Serilog.Events.LogEventLevel.Debug, outputTemplate: "[{Timestamp:yyyy-MM-dd HH:mm-ss.fff} {Level:u3}] {Message:lj}{NewLine}{Exception}")
                .WriteTo.File(Path.Combine(Environment.CurrentDirectory, "Logs", "Log.txt"), restrictedToMinimumLevel: Serilog.Events.LogEventLevel.Warning, rollingInterval: RollingInterval.Day, rollOnFileSizeLimit: true, encoding: Encoding.UTF8, retainedFileCountLimit: 1, fileSizeLimitBytes: 5 * 1024 * 1024)
                .CreateLogger();

            base.OnStartup(e);

            AppStart.LoadConfig();

            MainWindow login = new MainWindow();
            Application.Current.MainWindow = login;
            login.Show();
        }
    }
}
