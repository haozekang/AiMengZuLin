﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;

namespace AiMengZuLin.Enums
{
    /// <summary>
    /// 汽车状态枚举
    /// </summary>
    public enum CarStateEnum
    {
        暂停使用 = 1,
        可以使用 = 2,
        派单中 = 3,
        维修中 = 4,
        销售中 = 5,
    }

    /// <summary>
    /// 汽车状态枚举
    /// </summary>
    public enum CarStateSearchEnum
    {
        全部 = -1,
        暂停使用 = 1,
        可以使用 = 2,
        派单中 = 3,
        维修中 = 4,
        销售中 = 5,
    }
}
