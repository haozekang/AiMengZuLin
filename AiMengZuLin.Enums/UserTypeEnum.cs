﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;

namespace AiMengZuLin.Enums
{
    /// <summary>
    /// 用户类型枚举
    /// </summary>
    public enum UserTypeEnum
    {
        管理员 = 0,
        普通用户 = 1,
        司机 = 2,
        财务 = 3,
    }

    /// <summary>
    /// 用户类型枚举
    /// </summary>
    public enum UserTypeSearchEnum
    {
        全部 = -1,
        管理员 = 0,
        普通用户 = 1,
        司机 = 2,
        财务 = 3,
    }
}
