﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;

namespace AiMengZuLin.Enums
{
    /// <summary>
    /// 病例性别枚举
    /// </summary>
    public enum SexEnum
    {
        男 = 0,
        女 = 1,
        未知 = 2,
    }

    /// <summary>
    /// 病例性别枚举
    /// </summary>
    public enum SexSearchEnum
    {
        全部 = -1,
        男 = 0,
        女 = 1,
        未知 = 2,
    }
}
