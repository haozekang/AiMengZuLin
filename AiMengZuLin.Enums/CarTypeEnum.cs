﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;

namespace AiMengZuLin.Enums
{
    /// <summary>
    /// 汽车类型枚举
    /// </summary>
    public enum CarTypeEnum
    {
        轿车 = 1,
        SUV = 2,
        MPV = 3,
        客车 = 4,
    }

    /// <summary>
    /// 汽车类型枚举
    /// </summary>
    public enum CarTypeSearchEnum
    {
        全部 = -1,
        轿车 = 1,
        SUV = 2,
        MPV = 3,
        客车 = 4,
    }
}
