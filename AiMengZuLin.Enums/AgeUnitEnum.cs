﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace AiMengZuLin.Enums
{
    /// <summary>
    /// 年龄类型枚举
    /// </summary>
    public enum AgeUnitEnum
    {
        岁 = 0,
        月 = 1,
        日 = 2,
        时 = 3,
        分 = 4,
    }
}
