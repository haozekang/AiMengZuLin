﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;

namespace AiMengZuLin.Enums
{
    /// <summary>
    /// 用户状态枚举
    /// </summary>
    public enum UserStateEnum
    {
        正常使用 = 0,
        暂停使用 = 1,
        尚未激活 = 2,
        审核中 = 3,
    }

    /// <summary>
    /// 用户状态枚举
    /// </summary>
    public enum UserStateSearchEnum
    {
        全部 = -1,
        正常使用 = 0,
        暂停使用 = 1,
        尚未激活 = 2,
        审核中 = 3,
    }
}
