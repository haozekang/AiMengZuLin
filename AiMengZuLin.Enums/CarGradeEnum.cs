﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;

namespace AiMengZuLin.Enums
{
    /// <summary>
    /// 汽车等级枚举
    /// </summary>
    public enum CarGradeEnum
    {
        微型 = 1,
        小型 = 2,
        紧凑型 = 3,
        中型 = 4,
        中大型 = 5,
        大型 = 6,
    }

    /// <summary>
    /// 汽车等级枚举
    /// </summary>
    public enum CarGradeSearchEnum
    {
        全部 = -1,
        微型 = 1,
        小型 = 2,
        紧凑型 = 3,
        中型 = 4,
        中大型 = 5,
        大型 = 6,
    }
}
