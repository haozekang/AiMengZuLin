﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;

namespace AiMengZuLin.Enums
{
    /// <summary>
    /// 用户任务状态枚举
    /// </summary>
    public enum UserTaskStateEnum
    {
        暂无派单 = 0,
        派单中 = 1,
        已接收派单 = 2,
        执行派单中 = 3,
    }

    /// <summary>
    /// 用户任务状态枚举
    /// </summary>
    public enum UserTaskStateSearchEnum
    {
        全部 = -1,
        暂无派单 = 0,
        派单中 = 1,
        已接收派单 = 2,
        执行派单中 = 3,
    }
}
