﻿using AiMengZuLin.ExtendMethod;
using Serilog;
using Serilog.Core;
using SqlSugar;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AiMengZuLin.Database
{
    public class DbHelper
    {
        public static string DatabaseConnectionString { get; set; } = "";
        public static DbType DatabaseType { get; set; } = DbType.MySql;

        public static SqlSugarClient my
        {
            get
            {
                SqlSugarClient db = null;
                switch (DatabaseType)
                {
                    case DbType.MySql:
                        db = new SqlSugarClient(new ConnectionConfig()
                        {
                            ConnectionString = DatabaseConnectionString,
                            DbType = DbType.MySql,
                            IsAutoCloseConnection = true,
                            
                        });
                        break;
                    case DbType.SqlServer:
                        db = new SqlSugarClient(new ConnectionConfig()
                        {
                            ConnectionString = DatabaseConnectionString,
                            DbType = DbType.SqlServer,
                            IsAutoCloseConnection = true,
                        });
                        break;
                    case DbType.Oracle:
                        break;
                    case DbType.Sqlite:
                        break;
                    case DbType.PostgreSQL:
                        break;
                    case DbType.Dm:
                        break;
                    case DbType.Kdbndp:
                        break;
                    case DbType.Oscar:
                        break;
                }
                if (db != null)
                {
                    db.Aop.DataExecuting = (oldValue, entityInfo) =>
                    {
                        if (entityInfo.PropertyName == "CreateTime" && entityInfo.OperationType == DataFilterType.InsertByObject)
                        {
                            entityInfo.SetValue(DateTime.Now);
                        }
                        if (entityInfo.PropertyName == "UpdateTime" && entityInfo.OperationType == DataFilterType.UpdateByObject)
                        {
                            entityInfo.SetValue(DateTime.Now);
                        }
                    };
                }
#if DEBUG
                if (db != null)
                {
                    db.Aop.OnLogExecuting = (sql, args) =>
                    {
                    };
                    db.Aop.OnLogExecuted = (sql, args) =>
                    {
                        Log.Debug($"{sql}    执行耗时:{db.Ado.SqlExecutionTime}");
                        //执行时间超过1秒
                        if (db.Ado.SqlExecutionTime.TotalSeconds > 1)
                        {
                            //代码CS文件名
                            var fileName = db.Ado.SqlStackTrace.FirstFileName;
                            //代码行数
                            var fileLine = db.Ado.SqlStackTrace.FirstLine;
                            //方法名
                            var FirstMethodName = db.Ado.SqlStackTrace.FirstMethodName;
                            Log.Warning($"Filename:{fileName}  Line:{fileLine}  MethodName:{FirstMethodName}  耗时:{db.Ado.SqlExecutionTime}");
                        }
                    };
                    db.Aop.OnError = (exp) =>
                    {
                        Log.Error($"{exp}");
                    };
                }
#endif
                return db;
            }
        }
    }
}
