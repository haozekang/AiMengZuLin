﻿using AiMengZuLin.Enums;
using Microsoft.Toolkit.Mvvm.ComponentModel;
using SqlSugar;
using System;
using System.ComponentModel.DataAnnotations;

namespace AiMengZuLin.Entities
{
    [SugarTable("amzla_car")]
    public class Car : ObservableObject
    {
        [SugarColumn(ColumnName = "id", IsPrimaryKey = true, IsIdentity = true, ColumnDescription = "主键")]
        public long Id { set; get; }

        [SugarColumn(ColumnName = "brand", ColumnDescription = "汽车品牌"), 
            Required]
        public string Brand { set; get; }

        [SugarColumn(ColumnName = "name", ColumnDescription = "汽车名称"), 
            Required]
        public string Name { set; get; }

        [SugarColumn(ColumnName = "car_id", ColumnDescription = "车牌号"), 
            Required]
        public string CarId { set; get; }

        [SugarColumn(ColumnName = "type", ColumnDescription = "汽车分类")]
        public CarTypeEnum Type { set; get; } = CarTypeEnum.轿车;

        [SugarColumn(ColumnName = "grade", ColumnDescription = "汽车分类等级")]
        public CarGradeEnum Grade { set; get; } = CarGradeEnum.紧凑型;

        [SugarColumn(ColumnName = "describe", ColumnDescription = "描述")]
        public string Describe { set; get; }

        [SugarColumn(ColumnName = "buy_price", Length = 10, DecimalDigits = 2, ColumnDescription = "购买价格")]
        public decimal? BuyPrice { set; get; }

        [SugarColumn(ColumnName = "day_price", Length = 10, DecimalDigits = 2, ColumnDescription = "日租金")]
        public decimal? DayPrice { set; get; }

        [SugarColumn(ColumnName = "hour_price", Length = 10, DecimalDigits = 2, ColumnDescription = "时租金")]
        public decimal? HourPrice { set; get; }

        [SugarColumn(ColumnName = "remark", ColumnDescription = "备注")]
        public string Remark { set; get; }

        [SugarColumn(ColumnName = "buy_time", ColumnDescription = "购买时间")]
        public DateTime? BuyTime { set; get; }

        [SugarColumn(ColumnName = "state", ColumnDescription = "使用状态")]
        public CarStateEnum State { set; get; } = CarStateEnum.可以使用;

        [SugarColumn(ColumnName = "is_delete", IsNullable = false, ColumnDescription = "是否删除")]
        public bool? IsDelete { set; get; }

        [SugarColumn(ColumnName = "create_time", IsOnlyIgnoreInsert = true, ColumnDescription = "创建时间")]
        public DateTime? CreateTime { set; get; } = DateTime.Now;

        [SugarColumn(ColumnName = "update_time", IsOnlyIgnoreUpdate = true, ColumnDescription = "更新时间")]
        public DateTime? UpdateTime { set; get; }

        [SugarColumn(IsIgnore = true)]
        public User Driver { get; set; }
    }
}
