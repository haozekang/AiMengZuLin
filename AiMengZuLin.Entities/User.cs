﻿using AiMengZuLin.Enums;
using Microsoft.Toolkit.Mvvm.ComponentModel;
using SqlSugar;
using System;
using System.ComponentModel.DataAnnotations;

namespace AiMengZuLin.Entities
{
    [SugarTable("amzla_user")]
    public class User : ObservableObject
    {
        [SugarColumn(ColumnName = "id", IsPrimaryKey = true, IsIdentity = true, ColumnDescription = "主键")]
        public int Id { set; get; }

        [SugarColumn(ColumnName = "type", ColumnDescription = "用户类型")]
        public UserTypeEnum Type { set; get; } = UserTypeEnum.司机;

        [SugarColumn(ColumnName = "name", ColumnDescription = "姓名"),
            Required]
        public string Name { set; get; }

        [SugarColumn(ColumnName = "pinyin", ColumnDescription = "姓名拼音"),
            Required]
        public string Pinyin { set; get; }

        [SugarColumn(ColumnName = "sex", ColumnDescription = "性别"),
            Required]
        public SexEnum Sex { set; get; } = SexEnum.未知;

        [SugarColumn(ColumnName = "age", ColumnDescription = "年龄")]
        public int? Age { set; get; }

        [SugarColumn(ColumnName = "age_unit", ColumnDescription = "年龄单位", DefaultValue = "0")]
        public AgeUnitEnum AgeUnit { set; get; } = AgeUnitEnum.岁;

        [SugarColumn(ColumnName = "username", ColumnDescription = "账号"),
            Required]
        public string Username { set; get; }

        [SugarColumn(ColumnName = "password", ColumnDescription = "密码"),
            Required]
        public string Password { set; get; }

        [SugarColumn(ColumnName = "state", ColumnDescription = "用户状态"),
            Required]
        public UserStateEnum State { set; get; } = UserStateEnum.正常使用;

        [SugarColumn(ColumnName = "phone", ColumnDescription = "联系方式"),
            Required]
        public string Phone { set; get; }

        [SugarColumn(ColumnName = "remark", ColumnDescription = "备注")]
        public string Remark { set; get; }

        [SugarColumn(ColumnName = "create_time", ColumnDescription = "创建时间")]
        public DateTime? CreateTime { set; get; }

        [SugarColumn(ColumnName = "update_time", ColumnDescription = "更新时间")]
        public DateTime? UpdateTime { set; get; }

        [SugarColumn(ColumnName = "wx_app_openid", ColumnDescription = "微信小程序OpenId")]
        public string WxAppOpenId { set; get; }

        [SugarColumn(ColumnName = "wx_mp_openid", ColumnDescription = "微信公众号Id")]
        public string WxMpOpenId { set; get; }

        [SugarColumn(ColumnName = "is_delete", IsNullable = false, ColumnDescription = "是否删除")]
        public bool? IsDelete { set; get; }
    }
}
