﻿using AiMengZuLin.Basic;
using AiMengZuLin.ExtendMethod;
using MahApps.Metro.Controls;
using Microsoft.Toolkit.Mvvm.ComponentModel;
using Microsoft.Toolkit.Mvvm.Input;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;

namespace AiMengZuLin.ViewModels
{
    public class LoginWindowViewModel : ObservableRecipient
    {
        public string Title { get; set; } = $"{AppConfig.SystemTitle}";
        public string Username { get; set; } = $"";
        public string Password { get; set; } = $"";

        public LoginWindowViewModel()
        {
            LoginCommand = new AsyncRelayCommand<MetroWindow>(LoginAsync);
        }

        public IAsyncRelayCommand<MetroWindow> LoginCommand { get; }

        private async Task LoginAsync(MetroWindow page)
        {
            if (page == null)
            {
                return;
            }
            if (Username.IsBlank())
            {
                var txt_un = page.FindName("txt_un") as TextBox;
                txt_un?.Focus();
                return;
            }
            if (Password.IsBlank())
            {
                var txt_pw = page.FindName("txt_pw") as PasswordBox;
                txt_pw?.Focus();
                return;
            }
            await Task.Delay(2000);
            if (Username == "admin" && Password == "123")
            {
                page.Tag = true;
            }
            page.Close();
        }
    }
}
