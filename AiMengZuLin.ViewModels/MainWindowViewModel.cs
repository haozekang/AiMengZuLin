﻿using AiMengZuLin.Basic;
using AiMengZuLin.ExtendMethod;
using MahApps.Metro.Controls;
using Microsoft.Toolkit.Mvvm.ComponentModel;
using Microsoft.Toolkit.Mvvm.Input;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;

namespace AiMengZuLin.ViewModels
{
    public class MainWindowViewModel : ObservableRecipient
    {
        public string Title { get; set; } = $"{AppConfig.SystemTitle}";
        public string Name { get; set; } = $"";
        private Uri _pagePath;
        public Uri PagePath
        {
            get
            {
                return _pagePath;
            }
            set
            {
                SetProperty(ref _pagePath, value);
            }
        }

        public MainWindowViewModel()
        {
            LoadCommand = new AsyncRelayCommand<MetroWindow>(LoadAsync);
            HomeManagerCommand = new AsyncRelayCommand(HomeManagerAsync);
            CarManagerCommand = new AsyncRelayCommand(CarManagerAsync);
            DriverManagerCommand = new AsyncRelayCommand(DriverManagerAsync);
            TaskManagerCommand = new AsyncRelayCommand(TaskManagerAsync);
            OfficialWebsiteCommand = new AsyncRelayCommand(OfficialWebsiteAsync);
            PdfRenameCommand = new AsyncRelayCommand(PdfRenameAsync);
            DataConversionOneCommand = new AsyncRelayCommand(DataConversionOneAsync);
        }

        public IAsyncRelayCommand<MetroWindow> LoadCommand { get; }
        public IAsyncRelayCommand HomeManagerCommand { get; }
        public IAsyncRelayCommand CarManagerCommand { get; }
        public IAsyncRelayCommand DriverManagerCommand { get; }
        public IAsyncRelayCommand TaskManagerCommand { get; }
        public IAsyncRelayCommand OfficialWebsiteCommand { get; }
        public IAsyncRelayCommand PdfRenameCommand { get; }
        public IAsyncRelayCommand DataConversionOneCommand { get; }

        private async Task LoadAsync(MetroWindow page)
        {
            if (page == null)
            {
                return;
            }
            Name = $"管理员";
        }

        private async Task HomeManagerAsync()
        {
            //SetAllPageIsVisibleFalse();
            //HomeManagerIsVisible = true;
            PagePath = new Uri("pack://application:,,,/AiMengZuLin.Views;component/HomeManagerPage.xaml", UriKind.Absolute);
        }

        private async Task CarManagerAsync()
        {
            //SetAllPageIsVisibleFalse();
            //CarManagerIsVisible = true;
            PagePath = new Uri("pack://application:,,,/AiMengZuLin.Views;component/CarManagerPage.xaml", UriKind.Absolute);
        }

        private async Task DriverManagerAsync()
        {
            //SetAllPageIsVisibleFalse();
            //DriverManagerIsVisible = true;
            PagePath = new Uri("pack://application:,,,/AiMengZuLin.Views;component/DriverManagerPage.xaml", UriKind.Absolute);
        }

        private async Task TaskManagerAsync()
        {
            //SetAllPageIsVisibleFalse();
            //TaskManagerIsVisible = true;
            PagePath = new Uri("pack://application:,,,/AiMengZuLin.Views;component/TaskManagerPage.xaml", UriKind.Absolute);
        }

        private async Task OfficialWebsiteAsync()
        {
            Process.Start("explorer.exe", $"https://www.baidu.com/");
        }

        private async Task PdfRenameAsync()
        {
            //SetAllPageIsVisibleFalse();
            //PdfRenameIsVisible = true;
            PagePath = new Uri("pack://application:,,,/AiMengZuLin.Views;component/PdfRenamePage.xaml", UriKind.Absolute);
        }

        private async Task DataConversionOneAsync()
        {
            //SetAllPageIsVisibleFalse();
            //PdfRenameIsVisible = true;
            PagePath = new Uri("pack://application:,,,/AiMengZuLin.Views;component/DataConversionOnePage.xaml", UriKind.Absolute);
        }
    }
}
