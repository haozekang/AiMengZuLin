﻿using Microsoft.Toolkit.Mvvm.ComponentModel;
using Microsoft.Toolkit.Mvvm.Input;
using Ookii.Dialogs.Wpf;
using SixLabors.ImageSharp;
using Spire.Pdf;
using Spire.OCR;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Drawing.Imaging;
using SixLabors.ImageSharp.PixelFormats;
using SixLabors.ImageSharp.Processing;
using AiMengZuLin.Models;
using AiMengZuLin.ExtendMethod;
using AiMengZuLin.Entities;

namespace AiMengZuLin.ViewModels
{
    public class CarManagerPageViewModel : ObservableRecipient
    {
        public ObservableCollection<Car> Items { get; set; } = new ObservableCollection<Car>();
    }
}
