﻿using DryIoc;

namespace AiMengZuLin.ViewModels
{
    /// <summary>
    /// 这个类包含对应用程序中所有视图模型的静态引用，并提供绑定的入口点。
    /// </summary>
    public class ViewModelLocator
    {
        Container _container;

        /// <summary>
        /// 构造函数
        /// </summary>
        public ViewModelLocator()
        {
            //初始化容器
            _container = new Container();

            //注册到容器中
            _container.Register<LoginWindowViewModel>();
            _container.Register<MainWindowViewModel>();
            _container.Register<PdfRenamePageViewModel>();
            _container.Register<DataConversionOnePageViewModel>();
            _container.Register<DriverManagerPageViewModel>();
            _container.Register<CarManagerPageViewModel>();
        }

        public LoginWindowViewModel Login
        {
            get
            {
                return _container.Resolve<LoginWindowViewModel>();
            }
        }

        public MainWindowViewModel Main
        {
            get
            {
                return _container.Resolve<MainWindowViewModel>();
            }
        }

        public PdfRenamePageViewModel PdfRename
        {
            get
            {
                return _container.Resolve<PdfRenamePageViewModel>();
            }
        }

        public DataConversionOnePageViewModel DataConversionOne
        {
            get
            {
                return _container.Resolve<DataConversionOnePageViewModel>();
            }
        }

        public DriverManagerPageViewModel DriverManager
        {
            get
            {
                return _container.Resolve<DriverManagerPageViewModel>();
            }
        }

        public CarManagerPageViewModel CarManager
        {
            get
            {
                return _container.Resolve<CarManagerPageViewModel>();
            }
        }
    }
}