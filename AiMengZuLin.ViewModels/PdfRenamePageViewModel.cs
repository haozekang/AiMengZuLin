﻿using Microsoft.Toolkit.Mvvm.ComponentModel;
using Microsoft.Toolkit.Mvvm.Input;
using Ookii.Dialogs.Wpf;
using SixLabors.ImageSharp;
using Spire.Pdf;
using Spire.OCR;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Drawing.Imaging;
using SixLabors.ImageSharp.PixelFormats;
using SixLabors.ImageSharp.Processing;
using AiMengZuLin.Models;
using AiMengZuLin.ExtendMethod;

namespace AiMengZuLin.ViewModels
{
    public class PdfRenamePageViewModel : ObservableRecipient
    {
        public ObservableCollection<PdfRenameModel> PdfFileItems { get; set; } = new ObservableCollection<PdfRenameModel>();

        private string _dirPath = string.Empty;
        public string DirPath
        {
            get
            {
                return _dirPath;
            }
            set
            {
                SetProperty(ref _dirPath, value);
            }
        }

        public IAsyncRelayCommand SelectDirPathCommand { get; }
        public IAsyncRelayCommand GetDirFilesCommand { get; }
        public IAsyncRelayCommand OpenDirPathCommand { get; }
        public IAsyncRelayCommand StartRenameCommand { get; }
        public IAsyncRelayCommand<PdfRenameModel> RenameFileCommand { get; }
        public IAsyncRelayCommand RenameFilesCommand { get; }

        public PdfRenamePageViewModel()
        {
            SelectDirPathCommand = new AsyncRelayCommand(SelectDirPathAsync);
            GetDirFilesCommand = new AsyncRelayCommand(GetDirFilesAsync);
            OpenDirPathCommand = new AsyncRelayCommand(OpenDirPathAsync);
            StartRenameCommand = new AsyncRelayCommand(StartRenameAsync);
            RenameFileCommand = new AsyncRelayCommand<PdfRenameModel>(RenameFileAsync);
            RenameFilesCommand = new AsyncRelayCommand(RenameFilesAsync);
        }

        private async Task SelectDirPathAsync()
        {
            var dialog = new VistaFolderBrowserDialog();
            dialog.Description = "请选择PDF文件存放目录";
            dialog.UseDescriptionForTitle = true;
            if (!VistaFolderBrowserDialog.IsVistaFolderDialogSupported)
            {
                MessageBox.Show("仅支持Windows Vista及以上系统", "警告");
            }
            var flag = dialog.ShowDialog();
            if (flag != true)
            {
                return;
            }
            DirPath = dialog.SelectedPath;
            await GetDirFilesAsync();
        }

        private async Task GetDirFilesAsync()
        {
            PdfFileItems.Clear();
            if (!Directory.Exists(DirPath))
            {
                return;
            }
            var files = Directory.GetFiles(DirPath, "*.pdf");
            files.OrderBy(x => x).ToList().ForEach(x =>
            {
                var file = new FileInfo(x);
                if (file.Exists)
                {
                    PdfFileItems.Add(new PdfRenameModel
                    {
                        Name = file.Name,
                        FullName = file.FullName,
                    });
                }
            });
        }

        private async Task OpenDirPathAsync()
        {
            if (!Directory.Exists(DirPath))
            {
                return;
            }
            var p = Process.Start("explorer.exe", DirPath);
            p.Dispose();
        }

        private async Task StartRenameAsync()
        {
            foreach (var x in PdfFileItems)
            {
                using (PdfDocument pdf = new PdfDocument())
                {
                    pdf.LoadFromFile(x.FullName);
                    int pageIndex = 0;
                    for (pageIndex = 0; pageIndex < pdf.Pages.Count; pageIndex++)
                    {
                        var page = pdf.Pages[pageIndex];
                        var content = new StringBuilder();
                        var images = page.ExtractImages();
                        if (images.Length == 0)
                        {
                            return;
                        }
                        Debug.Write($"FileName:{x.Name} PageType:{page.Size}");
                        var image = images.First();
                        string i_a_path = Path.Combine(Environment.CurrentDirectory, $"temp_{Path.GetFileNameWithoutExtension(x.Name)}.png");
                        string i_c_path = Path.Combine(Environment.CurrentDirectory, $"temp_c_{Path.GetFileNameWithoutExtension(x.Name)}.png");
                        image.Save(i_a_path, ImageFormat.Png);
                        using (Image imageS = Image.Load(i_a_path))
                        {
                            imageS.Mutate(x =>
                                x.Crop(new Rectangle(3850, 2320, 200, 110))
                            );
                            imageS.Save(i_c_path);
                        }
                        x.OcrImage = new System.Windows.Media.Imaging.BitmapImage(new Uri(i_c_path, UriKind.Absolute));
                        using (OcrScanner scanner = new OcrScanner())
                        {
                            scanner.Scan(i_c_path);
                            content.Append(scanner.Text);
                            var _newName = content.ToString().ReplaceAll(new string[] { "\r", "\n", "图", "号", "串", "网", "电", "西", "四", "圆", "中" }, new string[] { "", "", "", "", "", "", "", "", "", "", "" });
                            x.NewName = $"{_newName}{Path.GetExtension(x.Name)}";
                            Debug.WriteLine($"OcrName:{_newName}");
                        }
                        break;
                    }
                }
                await Task.Delay(10);
            }
        }

        private async Task RenameFileAsync(PdfRenameModel model)
        {
            if (model == null)
            {
                return;
            }
            var file = new FileInfo(model.FullName);
            file.MoveTo(Path.Combine(Path.GetDirectoryName(model.FullName), model.NewName));
            model.FullName = Path.Combine(Path.GetDirectoryName(model.FullName), model.NewName);
            model.Name = model.NewName;
        }

        private async Task RenameFilesAsync()
        {
            foreach (var x in PdfFileItems)
            {
                var file = new FileInfo(x.FullName);
                file.MoveTo(Path.Combine(Path.GetDirectoryName(x.FullName), x.NewName));
                x.FullName = Path.Combine(Path.GetDirectoryName(x.FullName), x.NewName);
                x.Name = x.NewName;
            }
        }
    }
}
