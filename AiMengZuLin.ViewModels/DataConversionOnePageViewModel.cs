﻿using Microsoft.Toolkit.Mvvm.ComponentModel;
using Microsoft.Toolkit.Mvvm.Input;
using Ookii.Dialogs.Wpf;
using SixLabors.ImageSharp;
using Spire.Pdf;
using Spire.OCR;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Drawing.Imaging;
using SixLabors.ImageSharp.PixelFormats;
using SixLabors.ImageSharp.Processing;
using AiMengZuLin.Models;
using AiMengZuLin.ExtendMethod;
using AiMengZuLin.Enums;

namespace AiMengZuLin.ViewModels
{
    public class DataConversionOnePageViewModel : ObservableRecipient
    {
        public ObservableCollection<FileInfo> PreFileItems { get; set; } = new ObservableCollection<FileInfo>();
        public ObservableCollection<FileInfo> TemFileItems { get; set; } = new ObservableCollection<FileInfo>();
        public ObservableCollection<FileInfo> CsvFileItems { get; set; } = new ObservableCollection<FileInfo>();
        public Dictionary<string, List<DataConversionOneStationDataModel>> StationDataDictionary = new Dictionary<string, List<DataConversionOneStationDataModel>>();

        private string _preDirPath = string.Empty;
        public string PreDirPath
        {
            get
            {
                return _preDirPath;
            }
            set
            {
                SetProperty(ref _preDirPath, value);
            }
        }

        private string _temDirPath = string.Empty;
        public string TemDirPath
        {
            get
            {
                return _temDirPath;
            }
            set
            {
                SetProperty(ref _temDirPath, value);
            }
        }

        private int _stationCount = 0;
        public int StationCount
        {
            get
            {
                return _stationCount;
            }
            set
            {
                SetProperty(ref _stationCount, value);
            }
        }

        private int _stationIndex = 0;
        public int StationIndex
        {
            get
            {
                return _stationIndex;
            }
            set
            {
                SetProperty(ref _stationIndex, value);
            }
        }

        public IAsyncRelayCommand SelectPreDirPathCommand { get; }
        public IAsyncRelayCommand SelectTemDirPathCommand { get; }
        public IAsyncRelayCommand GetDirFilesCommand { get; }
        public IAsyncRelayCommand StartDataConversionCommand { get; }

        public DataConversionOnePageViewModel()
        {
            SelectPreDirPathCommand = new AsyncRelayCommand(SelectPreDirPathAsync);
            SelectTemDirPathCommand = new AsyncRelayCommand(SelectTemDirPathAsync);
            GetDirFilesCommand = new AsyncRelayCommand(GetDirFilesAsync);
            StartDataConversionCommand = new AsyncRelayCommand(StartDataConversionAsync);
        }

        private async Task SelectPreDirPathAsync()
        {
            var dialog = new VistaFolderBrowserDialog();
            dialog.Description = "请选择降水数据文件存放目录";
            dialog.UseDescriptionForTitle = true;
            if (!VistaFolderBrowserDialog.IsVistaFolderDialogSupported)
            {
                MessageBox.Show("仅支持Windows Vista及以上系统", "警告");
            }
            var flag = dialog.ShowDialog();
            if (flag != true)
            {
                return;
            }
            PreDirPath = dialog.SelectedPath;
            await GetDirFilesAsync();
        }

        private async Task SelectTemDirPathAsync()
        {
            var dialog = new VistaFolderBrowserDialog();
            dialog.Description = "请选择气温数据文件存放目录";
            dialog.UseDescriptionForTitle = true;
            if (!VistaFolderBrowserDialog.IsVistaFolderDialogSupported)
            {
                MessageBox.Show("仅支持Windows Vista及以上系统", "警告");
            }
            var flag = dialog.ShowDialog();
            if (flag != true)
            {
                return;
            }
            TemDirPath = dialog.SelectedPath;
            await GetDirFilesAsync();
        }

        private async Task GetDirFilesAsync()
        {
            PreFileItems.Clear();
            TemFileItems.Clear();
            if (Directory.Exists(PreDirPath))
            {
                var files = Directory.GetFiles(PreDirPath, "*.txt");
                files.OrderBy(x => x).ToList().ForEach(x =>
                {
                    var file = new FileInfo(x);
                    if (file.Exists)
                    {
                        PreFileItems.Add(file);
                    }
                });
            }
            if (Directory.Exists(TemDirPath))
            {
                var files = Directory.GetFiles(TemDirPath, "*.txt");
                files.OrderBy(x => x).ToList().ForEach(x =>
                {
                    var file = new FileInfo(x);
                    if (file.Exists)
                    {
                        TemFileItems.Add(file);
                    }
                });
            }
        }

        private async Task StartDataConversionAsync()
        {
            if (!Directory.Exists(Path.Combine(Environment.CurrentDirectory, "Error")))
            {
                Directory.CreateDirectory(Path.Combine(Environment.CurrentDirectory, "Error"));
            }
            var sn = 0;
            StationCount = 0;
            StationIndex = 0;
            CsvFileItems.Clear();
            StationDataDictionary.Clear();
            DataConversionOneStationDataModel item = null;
            var _decimal99_9 = decimal.Parse("-99.9");
            var decimal0_1 = decimal.Parse("0.1");
            var decimal0 = decimal.Parse("0");
            //var decimal32700 = decimal.Parse("32700");
            //var decimal32766 = decimal.Parse("32766");
            //var decimal999999 = decimal.Parse("999999");
            //var decimal999998 = decimal.Parse("999998");
            string[] stationDayDatas = new string[] { };
            try
            {
                //获取Pre信息
                foreach (var preTxtFile in PreFileItems)
                {
                    if (!File.Exists(preTxtFile.FullName))
                    {
                        continue;
                    }
                    var data = File.ReadAllText(preTxtFile.FullName);
                    var rowDatas = data.Split(new string[] { "\r", "\n", $"{Environment.NewLine}" }, StringSplitOptions.RemoveEmptyEntries);
                    int rowIndex = 0;
                    foreach (var rowData in rowDatas)
                    {
                        rowIndex++;
                        stationDayDatas = rowData.Split(new string[] { "\t", " " }, StringSplitOptions.RemoveEmptyEntries);
                        if (stationDayDatas.Length < 10)
                        {
                            File.WriteAllText(Path.Combine(Environment.CurrentDirectory, "Error", $"{Guid.NewGuid()}.txt"), $"{DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss")}: rowIndex:{rowIndex} row:{rowData} stationDayDatas.Length:{stationDayDatas.Length}");
                            continue;
                        }
                        if (!int.TryParse(stationDayDatas[0], out sn))
                        {
                            continue;
                        }
                        if (!StationDataDictionary.Keys.Contains(stationDayDatas[0]))
                        {
                            StationDataDictionary.Add(stationDayDatas[0], new List<DataConversionOneStationDataModel>());
                        }
                        item = new DataConversionOneStationDataModel
                        {
                            DataType = DataConversionOneDataTypeEnum.Pre,
                            Year = int.Parse(stationDayDatas[4]),
                            Month = int.Parse(stationDayDatas[5]),
                            Day = int.Parse(stationDayDatas[6]),
                            PreData20_20 = decimal.Parse(stationDayDatas[9]),
                        };
                        StationDataDictionary[stationDayDatas[0]].Add(item);
                        if (item.Year < 2020 || (item.Year == 2020 && item.Month < 4))
                        {
                            if (stationDayDatas[9].StringTrim() == "32700")
                            {
                                item.PreData20_20 = decimal0;
                            }
                            else if (stationDayDatas[9].StringTrim() == "32766")
                            {
                                item.PreData20_20 = _decimal99_9;
                            }
                            else
                            {
                                item.PreData20_20 = item.PreData20_20 * decimal0_1;
                            }
                        }
                        else if (item.Year > 2020 || (item.Year == 2020 && item.Month >= 4))
                        {
                            if (stationDayDatas[9].StringTrim() == "999999" || stationDayDatas[9].StringTrim() == "999998")
                            {
                                item.PreData20_20 = _decimal99_9;
                            }
                        }
                    }
                    StationCount = StationDataDictionary.Keys.Count;
                    await Task.Delay(5);
                }
                //获取Tem信息
                foreach (var temTxtFile in TemFileItems)
                {
                    if (!File.Exists(temTxtFile.FullName))
                    {
                        continue;
                    }
                    var data = File.ReadAllText(temTxtFile.FullName);
                    var rowDatas = data.Split(new string[] { "\r", "\n", $"{Environment.NewLine}" }, StringSplitOptions.RemoveEmptyEntries);
                    int rowIndex = 0;
                    foreach (var rowData in rowDatas)
                    {
                        rowIndex++;
                        stationDayDatas = rowData.Split(new string[] { "\t", " " }, StringSplitOptions.RemoveEmptyEntries);
                        if (stationDayDatas.Length < 10)
                        {
                            File.WriteAllText(Path.Combine(Environment.CurrentDirectory, "Error", $"{Guid.NewGuid()}.txt"), $"{DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss")}: rowIndex:{rowIndex} row:{rowData} stationDayDatas.Length:{stationDayDatas.Length}");
                            continue;
                        }
                        if (!int.TryParse(stationDayDatas[0], out sn))
                        {
                            continue;
                        }
                        if (!StationDataDictionary.Keys.Contains(stationDayDatas[0]))
                        {
                            StationDataDictionary.Add(stationDayDatas[0], new List<DataConversionOneStationDataModel>());
                        }
                        item = new DataConversionOneStationDataModel
                        {
                            DataType = DataConversionOneDataTypeEnum.Tem,
                            Year = int.Parse(stationDayDatas[4]),
                            Month = int.Parse(stationDayDatas[5]),
                            Day = int.Parse(stationDayDatas[6]),
                            TemMaxData = decimal.Parse(stationDayDatas[8]),
                            TemMinData = decimal.Parse(stationDayDatas[9]),
                        };
                        StationDataDictionary[stationDayDatas[0]].Add(item);
                        if (item.Year < 2020 || (item.Year == 2020 && item.Month < 4))
                        {
                            if (stationDayDatas[8].StringTrim() == "32766")
                            {
                                item.TemMaxData = _decimal99_9;
                            }
                            else
                            {
                                item.TemMaxData = item.TemMaxData * decimal0_1;
                            }
                            if (stationDayDatas[9].StringTrim() == "32766")
                            {
                                item.TemMinData = _decimal99_9;
                            }
                            else
                            {
                                item.TemMinData = item.TemMinData * decimal0_1;
                            }
                        }
                        else if (item.Year > 2020 || (item.Year == 2020 && item.Month >= 4))
                        {
                            if (stationDayDatas[8].StringTrim() == "999999" || stationDayDatas[8].StringTrim() == "999998")
                            {
                                item.TemMaxData = _decimal99_9;
                            }
                            if (stationDayDatas[9].StringTrim() == "999999" || stationDayDatas[9].StringTrim() == "999998")
                            {
                                item.TemMinData = _decimal99_9;
                            }
                        }
                    }
                    StationCount = StationDataDictionary.Keys.Count;
                    await Task.Delay(5);
                }
                var stationNumberList = StationDataDictionary.Keys.Distinct().OrderBy(x => x).ToList();
                StationCount = stationNumberList.Count;
                var yearMin = StationDataDictionary.Values.Select(x => x.Min(x => x.Year)).Min(x => x);
                var monthMin = StationDataDictionary.Values.Select(x => x.Min(x => x.Month)).Min(x => x);
                var yearMax = StationDataDictionary.Values.Select(x => x.Max(x => x.Year)).Max(x => x);
                var monthMax = StationDataDictionary.Values.Select(x => x.Max(x => x.Month)).Max(x => x);
                byte[] byteData = null;
                byte[] bomData = new byte[] { (byte)0xEF, (byte)0xBB, (byte)0xBF };
                if (!Directory.Exists(Path.Combine(Environment.CurrentDirectory, "Output")))
                {
                    Directory.CreateDirectory(Path.Combine(Environment.CurrentDirectory, "Output"));
                }
                await Task.Delay(10);
                foreach (var stationNumber in stationNumberList)
                {
                    StationIndex++;
                    var csvPath = Path.Combine(Environment.CurrentDirectory, "Output", $"{stationNumber}indcal.csv");
                    if (!File.Exists(csvPath))
                    {
                        if (!Directory.Exists(Path.GetDirectoryName(csvPath)))
                        {
                            Directory.CreateDirectory(Path.GetDirectoryName(csvPath));
                        }
                        using (var csv = new FileStream(csvPath, FileMode.CreateNew, FileAccess.Write, FileShare.Read))
                        {
                            csv.Write(bomData, 0, bomData.Length);
                            csv.Flush();
                            byteData = Encoding.UTF8.GetBytes($"Year, Month, Day, 20-20累计降水量, 日最高气温, 日最低气温{Environment.NewLine}");
                            csv.Write(byteData, 0, byteData.Length);
                            csv.Flush();
                        }
                    }
                    for (int y = yearMin; y <= yearMax; y++)
                    {
                        for (int m = monthMin; m <= monthMax; m++)
                        {
                            var days = DateTime.DaysInMonth(y, m);
                            for(int d = 1; d <= days; d++)
                            {
                                var stationPreData = StationDataDictionary[stationNumber].Where(x => x.DataType == DataConversionOneDataTypeEnum.Pre && x.Year == y && x.Month == m && x.Day == d).FirstOrDefault();
                                var stationTemData = StationDataDictionary[stationNumber].Where(x => x.DataType == DataConversionOneDataTypeEnum.Tem && x.Year == y && x.Month == m && x.Day == d).FirstOrDefault();
                                using (var csv = new FileStream(csvPath, FileMode.Append, FileAccess.Write, FileShare.Read))
                                {
                                    if (stationPreData == null && stationTemData == null)
                                    {
                                        byteData = Encoding.UTF8.GetBytes($"{y}, {m}, {d}, {_decimal99_9}, {_decimal99_9}, {_decimal99_9}{Environment.NewLine}");
                                    }
                                    else if (stationPreData == null && stationTemData != null)
                                    {
                                        byteData = Encoding.UTF8.GetBytes($"{y}, {m}, {d}, {_decimal99_9}, {stationTemData.TemMaxData}, {stationTemData.TemMinData}{Environment.NewLine}");
                                    }
                                    else if (stationPreData != null && stationTemData == null)
                                    {
                                        byteData = Encoding.UTF8.GetBytes($"{y}, {m}, {d}, {stationPreData.PreData20_20}, {_decimal99_9}, {_decimal99_9}{Environment.NewLine}");
                                    }
                                    else
                                    {
                                        byteData = Encoding.UTF8.GetBytes($"{y}, {m}, {d}, {stationPreData.PreData20_20}, {stationTemData.TemMaxData}, {stationTemData.TemMinData}{Environment.NewLine}");
                                    }
                                    csv.Write(byteData, 0, byteData.Length);
                                    csv.Flush();
                                }
                            }
                        }
                    }
                    if (File.Exists(csvPath))
                    {
                        CsvFileItems.Add(new FileInfo(csvPath));
                    }
                    await Task.Delay(10);
                }
                MessageBox.Show($"完成！");
            }
            catch (Exception e)
            {
                File.WriteAllText(Path.Combine(Environment.CurrentDirectory, "Error", $"{Guid.NewGuid()}.txt"), $"{DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss")}: {e}");
                //File.WriteAllText(Path.Combine(Environment.CurrentDirectory, "Error", $"{Guid.NewGuid()}.txt"), $"{DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss")}: {stationDayDatas[9]}");
            }
        }
    }
}
