﻿using Microsoft.Toolkit.Mvvm.ComponentModel;
using Microsoft.Toolkit.Mvvm.Input;
using Ookii.Dialogs.Wpf;
using SixLabors.ImageSharp;
using Spire.Pdf;
using Spire.OCR;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Drawing.Imaging;
using SixLabors.ImageSharp.PixelFormats;
using SixLabors.ImageSharp.Processing;
using AiMengZuLin.Models;
using AiMengZuLin.ExtendMethod;
using AiMengZuLin.Entities;
using AiMengZuLin.Enums;
using AiMengZuLin.Database;

namespace AiMengZuLin.ViewModels
{
    public class DriverManagerPageViewModel : ObservableRecipient
    {
        public ObservableCollection<User> Items { get; set; }
        public ObservableCollection<User> SelectedItems { get; set; }

        private string _name = string.Empty;
        public string Name
        {
            get
            {
                return _name;
            }
            set
            {
                SetProperty(ref _name, value);
            }
        }

        private SexSearchEnum _sex = SexSearchEnum.全部;
        public SexSearchEnum Sex
        {
            get
            {
                return _sex;
            }
            set
            {
                SetProperty(ref _sex, value);
            }
        }

        private string _phone = string.Empty;
        public string Phone
        {
            get
            {
                return _phone;
            }
            set
            {
                SetProperty(ref _phone, value);
            }
        }

        private UserStateSearchEnum _userState = UserStateSearchEnum.全部;
        public UserStateSearchEnum UserState
        {
            get
            {
                return _userState;
            }
            set
            {
                SetProperty(ref _userState, value);
            }
        }

        public SexSearchEnum SelectedSex { get; set; } = SexSearchEnum.全部;
        public SexSearchEnum[] SexItems { get; } = new SexSearchEnum[] { 
            SexSearchEnum.全部, 
            SexSearchEnum.男, 
            SexSearchEnum.女, 
            SexSearchEnum.未知 
        };

        public UserStateSearchEnum SelectedUserState { get; set; } = UserStateSearchEnum.全部;
        public UserStateSearchEnum[] UserStateItems { get; } = new UserStateSearchEnum[] { 
            UserStateSearchEnum.全部, 
            UserStateSearchEnum.正常使用, 
            UserStateSearchEnum.暂停使用, 
            UserStateSearchEnum.尚未激活, 
            UserStateSearchEnum.审核中 
        };

        public IAsyncRelayCommand SearchCommand { get; }
        public IAsyncRelayCommand OutputExcelCommand { get; }

        public DriverManagerPageViewModel()
        {
            GC.Collect();
            Items = new ObservableCollection<User>();
            SelectedItems = new ObservableCollection<User>();
            SearchCommand = new AsyncRelayCommand(SearchAsync);
            OutputExcelCommand = new AsyncRelayCommand(OutputExcelAsync);
        }

        private async Task SearchAsync()
        {
            Items.Clear();
            using (var my = DbHelper.my)
            {
                var query = my.Queryable<User>().Where(x => x.Type == UserTypeEnum.司机);
                if (Name.IsNotBlank())
                {
                    query = query.Where(x => x.Name.Contains(Name.StringTrim()));
                }
                if (Phone.IsNotBlank())
                {
                    query = query.Where(x => x.Phone == Phone);
                }
                if (SelectedSex != SexSearchEnum.全部)
                {
                    query = query.Where(x => x.Sex == (SexEnum)SelectedSex);
                }
                if (SelectedUserState != UserStateSearchEnum.全部)
                {
                    query = query.Where(x => x.State == (UserStateEnum)SelectedUserState);
                }
                var list = await query.ToListAsync();
                list.ForEach(x =>
                {
                    Items.Add(x);
                });
            }
        }

        private async Task OutputExcelAsync()
        {
        }
    }
}
